# Machine Problems (Kotlin Fundamentals)

---

This repository contains problems covering the Kotlin fundamentals for the December 2021 FFUF Spring Boot Bootcamp.
Below are the links to problems as well as the explanation for the solution.

## [Problem 1](src/main/kotlin/problem1)
## [Problem 2](src/main/kotlin/problem2)
## [Problem 3](src/main/kotlin/problem3)
## [Problem 4](src/main/kotlin/problem4)
## [Problem 5](src/main/kotlin/problem5)