# Problem 3

---

A personal phone directory contains room for first names and phone numbers for 30 people. Assign names and phone numbers
for the first 10 people. Prompt the user for a name, and if the name is found in the list, display the corresponding
phone number. If the name is not found in the list, prompt the user for a phone number, and add the new name and phone
number to the list. Continue to prompt the user for names until the user enters quit. After the lists are full
(containing 30 names), do not allow the user to add new entries.

## Solution

---

If we break down the problem, we can see that the following are the requirements for this program:

- We need to have a container for the names and phone numbers.
- We need to be able to add new names to this container.
- We need to be able to ask for user inputs and to continue asking until the container reaches a certain size.
- We need a way to check if a name already exists in the container.
- We need a way to quit the program anytime

Given those requirements, first thing to do is to create a container. We can use a `MutableMap` to contain both the name
and the phone number. By making it mutable, it will allow us to modify its contents.

We will fill the map up with 10 initial names and numbers.

```kotlin
val directory = mutableMapOf(
    "john" to "0917-123-9554",
    "aaron" to "0956-456-1475",
    "mark" to "0920-789-5471",
    "jerry" to "0954-147-9526",
    "kevin" to "0905-741-6358",
    "tom" to "0955-258-1975",
    "bob" to "0944-852-3842",
    "ryan" to "0970-369-6420",
    "elliot" to "0911-963-0004",
    "josh" to "0909-159-7747",
)
```

Next, we can define a couple of helper functions. The first one is a function to check if the name already exists in the
map `isContactExisting`.

```kotlin
fun isContactExisting(name: String, directory: MutableMap<String, String>): Boolean =
    directory.containsKey(name)
```

We are simply using the `containsKey` method to check if a name is already recorded in the map.

The next two functions are to simply capture user input for name and phone number, namely `askForName`
and `askForNumber`

```kotlin
fun askForName(): String {
    println("Please enter a name")
    return readln()
}

fun askForNumber(): String {
    println("Please enter their phone number.")
    return readln()
}
```

In order to continually ask the user for inputs, we will create a `while` loop. In this loop, we will continually ask
the user for names and numbers until the `directory` has 30 names in it.

```kotlin
while (directory.size < 30) {
}
```

Inside the loop, the first thing we do is to ask the user for a name. We will convert it to lowercase for consistency.

```kotlin
val name = askForName().lowercase()
```

Once we have a name, we can check it for certain conditions using the `when` keyword. If the user enters the word "quit"
as the name, we will break out of the `while` loop using the `break` keyword.

```kotlin
when {
    name == "quit" -> break // break loop if user typed "quit"
}
```

For the next condition, we will check if the name the user put in already exists in the map. If it does, then we will
simply print the name along with the number associated with it.

```kotlin
when {
    name == "quit" -> break // break loop if user typed "quit"
    isContactExisting(name, directory) -> println("$name's number is ${directory[name]}")
}
```

Finally, if the name is not yet in the map, we will ask the user again for a phone number. After getting the phone
number, we will make a new entry on the map with the name as the key and the phone number as the value. Then, we will
give feedback to the user saying that the name and phone number has been added to the map.

```kotlin
when {
    name == "quit" -> break // break loop if user typed "quit"
    isContactExisting(name, directory) -> println("$name's number is ${directory[name]}")
    else -> {
        val phone = askForNumber()
        directory[name] = phone
        println("$name has been added to the directory.")
    }
}
```
After the loop, we can finally print the contents of the directory map to the user.

### Final Output

```kotlin
fun main() {
    val directory = mutableMapOf(
        "john" to "0917-123-9554",
        "aaron" to "0956-456-1475",
        "mark" to "0920-789-5471",
        "jerry" to "0954-147-9526",
        "kevin" to "0905-741-6358",
        "tom" to "0955-258-1975",
        "bob" to "0944-852-3842",
        "ryan" to "0970-369-6420",
        "elliot" to "0911-963-0004",
        "josh" to "0909-159-7747",
    )

    // continue asking for names until the directory size is 30
    while (directory.size < 30) {
        val name = askForName().lowercase()
        when {
            name == "quit" -> break // break loop if user typed "quit"
            isContactExisting(name, directory) -> println("$name's number is ${directory[name]}")
            else -> {
                val phone = askForNumber()
                directory[name] = phone
                println("$name has been added to the directory.")
            }
        }
        println() // for spacing
    }
    println(directory)
}

fun isContactExisting(name: String, directory: MutableMap<String, String>): Boolean =
    directory.containsKey(name)

fun askForName(): String {
    println("Please enter a name")
    return readln()
}

fun askForNumber(): String {
    println("Please enter their phone number.")
    return readln()
}
```
