package problem3

// A personal phone directory contains room for first names and phone numbers for 30 people.
// Assign names and phone numbers for the first 10 people. Prompt the user for a name,
// and if the name is found in the list, display the corresponding phone number.
// If the name is not found in the list, prompt the user for a phone number,
// and add the new name and phone number to the list.
// Continue to prompt the user for names until the user enters quit.
// After the lists are full (containing 30 names), do not allow the user to add new entries.

fun main() {
    val directory = mutableMapOf(
        "john" to "0917-123-9554",
        "aaron" to "0956-456-1475",
        "mark" to "0920-789-5471",
        "jerry" to "0954-147-9526",
        "kevin" to "0905-741-6358",
        "tom" to "0955-258-1975",
        "bob" to "0944-852-3842",
        "ryan" to "0970-369-6420",
        "elliot" to "0911-963-0004",
        "josh" to "0909-159-7747",
    )

    // continue asking for names until the directory size is 30
    while (directory.size < 30) {
        val name = askForName().lowercase()
        when {
            name == "quit" -> break // break loop if user typed "quit"
            isContactExisting(name, directory) -> println("$name's number is ${directory[name]}")
            else -> {
                val phone = askForNumber()
                directory[name] = phone
                println("$name has been added to the directory.")
            }
        }
        println() // for spacing
    }
    println(directory)
}

fun isContactExisting(name: String, directory: MutableMap<String, String>): Boolean =
    directory.containsKey(name)

fun askForName(): String {
    println("Please enter a name")
    return readln()
}

fun askForNumber(): String {
    println("Please enter their phone number.")
    return readln()
}
