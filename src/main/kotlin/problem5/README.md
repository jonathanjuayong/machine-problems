# Problem 5

---

Write an application that allows a user to enter the names and birthdates of up to 10 friends. Continue to prompt the
user for names and birthdates until the user enters the sentinel value ZZZ for a name or has entered 10 names, whichever
comes first. When the user is finished entering names, produce a count of how many names were entered, and then display
the names. In a loop, continuously ask the user to type one of the names and display the corresponding birthdate or an
error message if the name has not been previously entered. The loop continues until the user enters ZZZ for a name.

## Solution

---

For this problem, we need to think of a container to contain the names and birthdates of our friends. We can use `map`
the same way we did for the previous problems but for this one, we will use a different data structure: `Pair`. A `Pair`
contains only two items, and we can define a different type for each item. But for this one, we will make them
both `String`.

Each `Pair<String, String>` will represent one friend and their corresponding birthday. We can put these `Pair`s inside
a
`MutableList`. By choosing a `MutableList`, we can do modifications on it like add or subtracting an item.

First, let's initialize an empty `MutableList` of type `MutableList<Pair<String, String>>`

```kotlin
// initialize friends list
val friendsList = mutableListOf<Pair<String, String>>()
```

Next, we will populate that list by asking the user for inputs. We'll keep asking them for input until:

- `friendsList` has 10 items inside, or
- if the user input is equal to "ZZZ"

First, we will create a helper function `askForNameAndBirthday` that will prompt the user for a name and a birthday. It
will return a `Pair<String, String>`.

```kotlin
// prompts the user for a name and a bday and returns them in a Pair
fun askForNameAndBirthday(): Pair<String, String> {
    println("Please enter a name.")
    val name = readln()

    println("Please enter a birthdate.")
    val bday = readln()

    return Pair(name, bday)
}
```

Next, we will create another helper function `addFriendsLoop` that will call `askForNameAndBirthday` to keep asking the
user for inputs and stop based on those two conditions above. The function will only take one parameter, which is
the `friendsList`
we created earlier. It will not have a return value because we will be modifying the `friendsList` variable by adding
items in it.

```kotlin
fun addFriendsLoop(friendsList: MutableList<Pair<String, String>>)
```

In order to create the loop, we will use `while`. The condition we'll be checking is if `friendsList` does not have 10
items inside it yet.

```kotlin
while (friendsList.size != 10)
```

Inside the loop, we call `askForNameAndBirthday` to capture user input. One advantage of using `Pair`s is that we can
destructure their values into variable. For this case, we destructure them into `name` and `bday`

```kotlin
// prompt user for name and birthday
val (name, bday) = askForNameAndBirthday()
```

Once we get the `name`, we can check its contents for conditions using `when`. The first condition we will check for is
if
`name` is equal to "ZZZ". In that case, we will terminate the loop early

```kotlin
name == "ZZZ" -> println("You have exited the loop")
```

If `name` is empty or is blank, we will show an error message and then ask the user again for a valid input. We'll do
this through recursion by calling `addFriendsLoop` with the same arguments.

```kotlin
// ask again if user inputs a blank or empty name
isEmptyOrBlank(name) || isEmptyOrBlank(bday) -> {
    println("Please do not leave the name or birthday blank")
    addFriendsLoop(friendsList)
}
```

Else, if `name` passed the test, we will add a `Pair` of `name` and `bday` to `friendsList`. We will also ask the user
again for input through recursion to continue the loop.

```kotlin
else -> {
    friendsList.add(Pair(name.trim().lowercase(), bday))
    addFriendsLoop(friendsList)
}
```

Lastly, we will put the `break` keyword after the `when` expression in order to prevent the function from looping
infinitely.

Here is the complete function body:

```kotlin
fun addFriendsLoop(friendsList: MutableList<Pair<String, String>>) {
    // stop loop if friends list has already 10 names
    while (friendsList.size != 10) {

        // prompt user for name and birthday
        val (name, bday) = askForNameAndBirthday()
        when {
            // quit loop if user enters "ZZZ"
            name == "ZZZ" -> println("You have exited the loop")

            // ask again if user inputs a blank or empty name
            isEmptyOrBlank(name) || isEmptyOrBlank(bday) -> {
                println("Please do not leave the name or birthday blank")
                addFriendsLoop(friendsList)
            }

            // add friend to friendsList. remove whitespace before and after
            // convert to lowercase for consistency
            else -> {
                friendsList.add(Pair(name.trim().lowercase(), bday))
                addFriendsLoop(friendsList)
            }
        }
        break
    }
}
```

Going back to the `main` function, we can now use `addFriendsLoop` to prompt the user for inputs.

```kotlin
fun main() {
    // initialize friends list
    val friendsList = mutableListOf<Pair<String, String>>()

    // keep asking user for names until friendsList has 10 names or if user input is "ZZZ"
    addFriendsLoop(friendsList)
    ...
```

After the `addFriendsLoop`, we can display to the user the number of friends on their list using the `size` property and
print out `friendsList`.

```kotlin
// display number of friends
println("You have ${friendsList.size} friends on your list.")

// print out all friends in friendsList
displayNames(friendsList)
```

For the next part of the program, we will have to enter another loop wherein we will ask the user for a name and we will
check if the name exists in `friendsList`. If it does, we will print out their birthday. If it doesn't, we will print an
error message and then ask them again for a different name.

First, we will define a helper function `askForFriendName` which simply prompts the user for a name.

```kotlin
fun askForFriendName(): String {
    println("Please enter the name of your friend.")
    return readln()
}
```

Then, we will create another helper function `lookupFriendBday` to get the birthday of a friend in `friendsList`. This
function will accept two parameters:

- name: String
- friendsList: MutableList<Pair<String, String>>

It will return `String?` since the friend may or may not exist in the `friendsList`

```kotlin
fun lookupFriendBday(name: String, friendsList: MutableList<Pair<String, String>>): String?
```

The way we will do the lookup is by using the `find` method on the `list`. `find` accepts a lambda function which has
one parameter and returns a `Boolean`. It will return the item in the list if it passes the condition on the lambda
function. It will return `null` if none of the items in the list pass the lambda function.

We will use the `first` property of `it` in the lambda function, which refers to the name of the friend, and compare it
with the name from the parameter. If it is `true`, we will use the `second` property of the `Pair` which in this case
will return the birthday of said friend.

```kotlin
fun lookupFriendBday(name: String, friendsList: MutableList<Pair<String, String>>): String? {
    val friend = friendsList.find { it.first == name.lowercase() }
    return friend?.second
}
```

After creating those helper functions, we will now create function `friendLookupLoop` that will loop in asking the user
for input and terminate only if the user enters "ZZZ". The function only has one parameter, which is the `friendsList`.

```kotlin
fun friendLookupLoop(friendsList: MutableList<Pair<String, String>>)
```

First thing to do in this function is to prompt the user for a name. We will use `askForFriendName` to do so.

```kotlin
// prompt user for a name
val friendName = askForFriendName()
```

Once we get the name, we will again use `when` to check certain conditions

```kotlin
when {
        ...
}
```

The first condition to check is if the user input is blank or empty. In that case, we will ask the user again for a
valid input through recursion.

```kotlin
// ask again if user input is blank or is empty
isEmptyOrBlank(friendName) -> {
    println("Please do not leave the name blank.")
    friendLookupLoop(friendsList)
}
```

The next condition to check is if the user input is equal to "ZZZ". For that, we will simply do nothing but print out a
message since doing so will naturally end the program. The only way we will loop in this function is through recursion.

```kotlin
// quit loop if user enters ZZZ
friendName == "ZZZ" -> {
    println("You have exited the loop.")
}
```

Next, if the user input is a valid name, we will perform a lookup of the name on the `friendsList` and return the
corresponding birthday.

```kotlin
else -> {
    // lookup friend's birthday in friendsList
    val friendBday = lookupFriendBday(friendName, friendsList)
    ...
```

If `friendBday` is `null`, that means that the friend does not exist in `friendsList` In that case, we will print an
error message and prompt the user again for a valid input through recursion.

```kotlin
if (friendBday == null) {
    println("Error: Friend with that name does not exist.")
    friendLookupLoop(friendsList)
}
```

Else, if the friend exists in `friendsList`, we will simply print out the birthday of said friend and ask the user again
for another name.

```kotlin
else { // print friends name and birthday. recurse to continue loop
    println("$friendName's birthday is $friendBday")
    friendLookupLoop(friendsList)
}
```

Below is the full function body:

```kotlin
fun friendLookupLoop(friendsList: MutableList<Pair<String, String>>) {
    // prompt user for a name
    val friendName = askForFriendName()
    when {
        // ask again if user input is blank or is empty
        isEmptyOrBlank(friendName) -> {
            println("Please do not leave the name blank.")
            friendLookupLoop(friendsList)
        }

        // quit loop if user enters ZZZ
        friendName == "ZZZ" -> {
            println("You have exited the loop.")
        }
        else -> {
            // lookup friend's birthday in friendsList
            val friendBday = lookupFriendBday(friendName, friendsList)

            // if friend does not exist in the list, show error message
            // and ask again
            if (friendBday == null) {
                println("Error: Friend with that name does not exist.")
                friendLookupLoop(friendsList)
            } else { // print friends name and birthday. recurse to continue loop
                println("$friendName's birthday is $friendBday")
                friendLookupLoop(friendsList)
            }
        }
    }
}
```

Going back to `main`, we can simply call this function at the end.

```kotlin
// ask for name in friendList and lookup their birthday.
// show error if name does not exist in the friendsList.
// quit loop if user types "ZZZ"
friendLookupLoop(friendsList)
```

### Final Output

```kotlin
fun main() {
    // initialize friends list
    val friendsList = mutableListOf<Pair<String, String>>()

    // keep asking user for names until friendsList has 10 names or if user input is "ZZZ"
    addFriendsLoop(friendsList)

    // display number of friends
    println("You have ${friendsList.size} friends on your list.")

    // print out all friends in friendsList
    displayNames(friendsList)

    // ask for name in friendList and lookup their birthday.
    // show error if name does not exist in the friendsList.
    // quit loop if user types "ZZZ"
    friendLookupLoop(friendsList)
}

// prompts the user for a name and a bday and returns them in a Pair
fun askForNameAndBirthday(): Pair<String, String> {
    println("Please enter a name.")
    val name = readln()

    println("Please enter a birthdate.")
    val bday = readln()

    return Pair(name, bday)
}

// goes through each item in the friendsList and displays their name and birthday
fun displayNames(friendsList: MutableList<Pair<String, String>>) {
    friendsList.forEach {(name, bday) ->
        println("$name - $bday")
    }
}

fun askForFriendName(): String {
    println("Please enter the name of your friend.")
    return readln()
}

// look up birthday of friend by using find method.
// returns null if name does not exist
fun lookupFriendBday(name: String, friendsList: MutableList<Pair<String, String>>): String? {
    val friend = friendsList.find {it.first == name.lowercase()}
    return friend?.second
}

// checks if user input is empty or is simply whitespace
fun isEmptyOrBlank(string: String): Boolean =
    string.isEmpty() || string.isBlank()

fun addFriendsLoop(friendsList: MutableList<Pair<String, String>>) {
    // stop loop if friends list has already 10 names
    while (friendsList.size != 10) {

        // prompt user for name and birthday
        val (name, bday) = askForNameAndBirthday()
        when {
            // quit loop if user enters "ZZZ"
            name == "ZZZ" -> println("You have exited the loop")

            // ask again if user inputs a blank or empty name
            isEmptyOrBlank(name) || isEmptyOrBlank(bday) -> {
                println("Please do not leave the name or birthday blank")
                addFriendsLoop(friendsList)
            }

            // add friend to friendsList. remove whitespace before and after
            // convert to lowercase for consistency
            else -> {
                friendsList.add(Pair(name.trim().lowercase(), bday))
                addFriendsLoop(friendsList)
            }
        }
        break
    }
}

fun friendLookupLoop(friendsList: MutableList<Pair<String, String>>) {
    // prompt user for a name
    val friendName = askForFriendName()
    when {
        // ask again if user input is blank or is empty
        isEmptyOrBlank(friendName) -> {
            println("Please do not leave the name blank.")
            friendLookupLoop(friendsList)
        }

        // quit loop if user enters ZZZ
        friendName == "ZZZ" -> {
            println("You have exited the loop.")
        }
        else -> {
            // lookup friend's birthday in friendsList
            val friendBday = lookupFriendBday(friendName, friendsList)

            // if friend does not exist in the list, show error message
            // and ask again
            if (friendBday == null) {
                println("Error: Friend with that name does not exist.")
                friendLookupLoop(friendsList)
            } else { // print friends name and birthday. recurse to continue loop
                println("$friendName's birthday is $friendBday")
                friendLookupLoop(friendsList)
            }
        }
    }
}
```
