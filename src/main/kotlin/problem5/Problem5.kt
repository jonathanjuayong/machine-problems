package problem5

// Write an application that allows a user to enter the names and birthdates of up to 10 friends.
// Continue to prompt the user for names and birthdates until the user enters the sentinel value ZZZ
// for a name or has entered 10 names, whichever comes first. When the user is finished entering names,
// produce a count of how many names were entered, and then display the names. In a loop, continuously ask
// the user to type one of the names and display the corresponding birthdate or an error message if the name
// has not been previously entered. The loop continues until the user enters ZZZ for a name.

fun main() {
    // initialize friends list
    val friendsList = mutableListOf<Pair<String, String>>()

    // keep asking user for names until friendsList has 10 names or if user input is "ZZZ"
    addFriendsLoop(friendsList)

    // display number of friends
    println("You have ${friendsList.size} friends on your list.")

    // print out all friends in friendsList
    displayNames(friendsList)

    // ask for name in friendList and lookup their birthday.
    // show error if name does not exist in the friendsList.
    // quit loop if user types "ZZZ"
    friendLookupLoop(friendsList)
}

// prompts the user for a name and a bday and returns them in a Pair
fun askForNameAndBirthday(): Pair<String, String> {
    println("Please enter a name.")
    val name = readln()

    println("Please enter a birthdate.")
    val bday = readln()

    return Pair(name, bday)
}

// goes through each item in the friendsList and displays their name and birthday
fun displayNames(friendsList: MutableList<Pair<String, String>>) {
    friendsList.forEach {(name, bday) ->
        println("$name - $bday")
    }
}

fun askForFriendName(): String {
    println("Please enter the name of your friend.")
    return readln()
}

// look up birthday of friend by using find method.
// returns null if name does not exist
fun lookupFriendBday(name: String, friendsList: MutableList<Pair<String, String>>): String? {
    val friend = friendsList.find {it.first == name.lowercase()}
    return friend?.second
}

// checks if user input is empty or is simply whitespace
fun isEmptyOrBlank(string: String): Boolean =
    string.isEmpty() || string.isBlank()

fun addFriendsLoop(friendsList: MutableList<Pair<String, String>>) {
    // stop loop if friends list has already 10 names
    while (friendsList.size != 10) {

        // prompt user for name and birthday
        val (name, bday) = askForNameAndBirthday()
        when {
            // quit loop if user enters "ZZZ"
            name == "ZZZ" -> println("You have exited the loop")

            // ask again if user inputs a blank or empty name
            isEmptyOrBlank(name) || isEmptyOrBlank(bday) -> {
                println("Please do not leave the name or birthday blank")
                addFriendsLoop(friendsList)
            }

            // add friend to friendsList. remove whitespace before and after
            // convert to lowercase for consistency
            else -> {
                friendsList.add(Pair(name.trim().lowercase(), bday))
                addFriendsLoop(friendsList)
            }
        }
        break
    }
}

fun friendLookupLoop(friendsList: MutableList<Pair<String, String>>) {
    // prompt user for a name
    val friendName = askForFriendName()
    when {
        // ask again if user input is blank or is empty
        isEmptyOrBlank(friendName) -> {
            println("Please do not leave the name blank.")
            friendLookupLoop(friendsList)
        }

        // quit loop if user enters ZZZ
        friendName == "ZZZ" -> {
            println("You have exited the loop.")
        }
        else -> {
            // lookup friend's birthday in friendsList
            val friendBday = lookupFriendBday(friendName, friendsList)

            // if friend does not exist in the list, show error message
            // and ask again
            if (friendBday == null) {
                println("Error: Friend with that name does not exist.")
                friendLookupLoop(friendsList)
            } else { // print friends name and birthday. recurse to continue loop
                println("$friendName's birthday is $friendBday")
                friendLookupLoop(friendsList)
            }
        }
    }
}

