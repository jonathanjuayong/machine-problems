package problem4

// Write an application that contains an array of 10 multiple-choice quiz questions related to your favorite hobby.
// Each question contains three answer choices. Also create an array that holds the correct answer to each
// question—A, B, or C. Display each question and verify that the user enters only A, B, or C as the
// answer—if not, keep prompting the user until a valid response is entered. If the user responds to a
// question correctly, display Correct!; otherwise, display The correct answer is ... and the letter of the
// correct answer. After the user answers all the questions, display the number of correct and incorrect answers.

fun main() {
    // initialize array of questions
    val questions = arrayOf(
        questionCreator("What is Roger Federer's home country?", "France", "Switzerland", "Germany", "b"),
        questionCreator("Who has won the most women's Wimbledon singles titles?", "Martina Navratilova", "Serena Williams", "Chris Evert", "a"),
        questionCreator("How many French Open singles titles has Rafael Nadal won", "15", "14", "13", "c"),
        questionCreator("Who was the youngest and the first unseeded player to win the men's singles at Wimbledon?", "Michael Chang", "Boris Becker", "Jim Courier", "b"),
        questionCreator("Novak Djokovic turned professional in which year?", "2002", "2003", "2004", "b"),
        questionCreator("How many Grand Slams has Venus Williams won?", "7", "8", "9", "a"),
        questionCreator("Who won the women's French Open in 2019?", "Ashleigh Barty", "Serena Williams", "Naomi Osaka", "a"),
        questionCreator("Who ended a 77-year wait for a British men's Wimbledon champion in 2013?", "Cam Norrie", "Dan Evans", "Andy Murray", "c"),
        questionCreator("What is Rafael Nadal's hometown in Spain?", "Mallorca", "Barcelona", "Pamplona", "a"),
        questionCreator("Which player competed in the men’s singles finals match at the US Open 8 years consecutively?", "Boris Becker", "Ivan Lendl", "Bjorn Borg", "b"),
    )

    // user score
    var score = 0

    // loop over each question and ask the user
    questions.forEach { item ->
        // separate the values in item
        val question = item["question"]!!
        val a = item["a"]!!
        val b = item["b"]!!
        val c = item["c"]!!
        val correctAnswer = item["correct"]!!

        // prompt user with question
        val userAnswer = askQuestion(question, a, b, c)

        // check if user's answer is correct
        if (userAnswer == correctAnswer) {
            println("Correct!")
            score++
        } else {
            println("Sorry! The correct answer is $correctAnswer")
        }
        println() // add extra space
    }
    println("Your final score is: $score out of ${questions.size}")
    val feedback = when (score) {
        in 0..5 -> "Better luck next time!"
        in 6..9 -> "Not bad!"
        else -> "Awesome! You got a perfect score!"
    }
    println(feedback)
    println("Thank you for playing!")
}

fun askQuestion(question: String, a: String, b: String, c: String): String {
    // display the question
    println(question)
    println("(a) - $a")
    println("(b) - $b")
    println("(c) - $c")

    // get user answer
    val answer = readln().lowercase()
    return when {
        // recurse if invalid answer
        answer.isBlank() || answer.isEmpty() -> {
            println("Please enter an answer")
            askQuestion(question, a, b, c)
        }
        answer !in "abc" -> {
            println("Please select between a, b, or c")
            askQuestion(question, a, b, c)
        }

        // return if valid answer
        else -> {
            println("Your answer is: $answer")
            return answer.trim()
        }
    }
}

// function to create a question in the form of a map
fun questionCreator(
    question: String,
    a: String,
    b: String,
    c: String,
    correctAnswer: String
): Map<String, String> =
    mapOf(
        "question" to question,
        "a" to a,
        "b" to b,
        "c" to c,
        "correct" to correctAnswer
    )


