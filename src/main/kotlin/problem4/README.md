# Problem 4

---

Write an application that contains an array of 10 multiple-choice quiz questions related to your favorite hobby. Each
question contains three answer choices. Also create an array that holds the correct answer to each question—A, B, or C.
Display each question and verify that the user enters only A, B, or C as the answer—if not, keep prompting the user
until a valid response is entered. If the user responds to a question correctly, display Correct!; otherwise, display
The correct answer is ... and the letter of the correct answer. After the user answers all the questions, display the
number of correct and incorrect answers.

## Solution

---

For this problem, we must create a container for all the questions that will be asked to the user. We can use an `array`
so that we can iterate over each of them later. We also need to decide what kind of container or data structure we're
going to use for each individual question. We can simply store them in `String`s but it would be a bit difficult to
handle them. If we break it down to smaller components, a question has the following parts:

- The question itself.
- The choices (a, b, c)
- The correct answer e.g. "a"

To make it simpler but manageable, we can use `map` to contain each individual question that we will create. We will
create a `map` of type `Map<String, String>`. Below is an example:

```kotlin
mapOf(
    "question" to "What is 1 + 1",
    "a" to "1",
    "b" to "2",
    "c" to "3",
    "correct" to "b"
)
```

In order to further simplify the creation of questions, we can create a function called `questionCreator` to help us.
That way, we won't have to type out each key value pair.

```kotlin
// function to create a question in the form of a map
fun questionCreator(
    question: String,
    a: String,
    b: String,
    c: String,
    correctAnswer: String
): Map<String, String> =
    mapOf(
        "question" to question,
        "a" to a,
        "b" to b,
        "c" to c,
        "correct" to correctAnswer
    )
```

We can now create an `array` of questions. The type signature of the `array` will be `Array<Map <String, String>>`

```kotlin
// initialize array of questions
val questions = arrayOf(
    questionCreator("What is Roger Federer's home country?", "France", "Switzerland", "Germany", "b"),
    questionCreator(
        "Who has won the most women's Wimbledon singles titles?",
        "Martina Navratilova",
        "Serena Williams",
        "Chris Evert",
        "a"
    ),
    questionCreator("How many French Open singles titles has Rafael Nadal won", "15", "14", "13", "c"),
    questionCreator(
        "Who was the youngest and the first unseeded player to win the men's singles at Wimbledon?",
        "Michael Chang",
        "Boris Becker",
        "Jim Courier",
        "b"
    ),
    questionCreator("Novak Djokovic turned professional in which year?", "2002", "2003", "2004", "b"),
    questionCreator("How many Grand Slams has Venus Williams won?", "7", "8", "9", "a"),
    questionCreator(
        "Who won the women's French Open in 2019?",
        "Ashleigh Barty",
        "Serena Williams",
        "Naomi Osaka",
        "a"
    ),
    questionCreator(
        "Who ended a 77-year wait for a British men's Wimbledon champion in 2013?",
        "Cam Norrie",
        "Dan Evans",
        "Andy Murray",
        "c"
    ),
    questionCreator("What is Rafael Nadal's hometown in Spain?", "Mallorca", "Barcelona", "Pamplona", "a"),
    questionCreator(
        "Which player competed in the men’s singles finals match at the US Open 8 years consecutively?",
        "Boris Becker",
        "Ivan Lendl",
        "Bjorn Borg",
        "b"
    ),
)
```

Next, we will create a variable that will hold the score of the user. Since we know that we will be modifying this
variable later, we will use `var` instead of `val`. This will keep track of the correct answers the player will have.

```kotlin
// user score
var score = 0
```

Before we start iterating over the array, we can create another helper function that will ask the user the question and
capture their response. We will name the function `askQuestion`. This function will accept 4 parameters of
type `String`:

- question: String
- a: String
- b: String
- c: String

The return type of this function, which will be the input of the user, will also be a `String`. Below is the complete
type signature of the function.

```kotlin
fun askQuestion(question: String, a: String, b: String, c: String): String
```

The first part of the function body will simply print out the question and the three choices: a, b, c using string
templates.

```kotlin
// display the question
println(question)
println("(a) - $a")
println("(b) - $b")
println("(c) - $c")
```

Then, we will capture the response of the user using `readln()` and convert it to lowercase()

```kotlin
// get user answer
val answer = readln().lowercase()
```

After getting the input of the user, we will have to validate it on certain conditions. We will use the `when` keyword
to check for multiple conditions. The first condition we will check for is if the user's answer is empty or blank. For
that, we will use the string methods `isEmpty()` and `isBlank`. If the answer is empty or blank, we will prompt the user
again for an input until they make a valid response. We will do this through recursion by simply calling
the `askQuestion`
function with the same arguments.

```kotlin
// recurse if invalid answer
answer.isBlank() || answer.isEmpty() -> {
    println("Please enter an answer")
    askQuestion(question, a, b, c)
}
```

We will do the same thing if the user's answer is not "a", "b", or "c".

```kotlin
answer !in "abc" -> {
    println("Please select between a, b, or c")
    askQuestion(question, a, b, c)
}
```

Once, we get a valid answer, we can simply return it. We use `trim()` to remove leading or trailing whitespaces.

```kotlin
else -> {
    println("Your answer is: $answer")
    return answer.trim()
}
```

Below is the full function body.

```kotlin
fun askQuestion(question: String, a: String, b: String, c: String): String {
    // display the question
    println(question)
    println("(a) - $a")
    println("(b) - $b")
    println("(c) - $c")

    // get user answer
    val answer = readln().lowercase()
    return when {
        // recurse if invalid answer
        answer.isBlank() || answer.isEmpty() -> {
            println("Please enter an answer")
            askQuestion(question, a, b, c)
        }
        answer !in "abc" -> {
            println("Please select between a, b, or c")
            askQuestion(question, a, b, c)
        }

        // return if valid answer
        else -> {
            println("Your answer is: $answer")
            return answer.trim()
        }
    }
}
```

We can now iterate over the array of questions. We will do so by using `forEach`. In the lambda function, we need to
extract the values from the question.

```kotlin
questions.forEach { item ->
    // separate the values in item
    val question = item["question"]!!
    val a = item["a"]!!
    val b = item["b"]!!
    val c = item["c"]!!
    val correctAnswer = item["correct"]!!
    ...
```

After extracting the values, we can use it to call `askQuestion` and store the user's input on a variable.

```kotlin
// prompt user with question
val userAnswer = askQuestion(question, a, b, c)
```

We can now compare the user's answer to the correct answer to the question. If the user answer and the correct answer is
the same, we increment the `score` variable by 1. Else, we display to the user the correct answer and move on to the
next question.

```kotlin
// check if user's answer is correct
if (userAnswer == correctAnswer) {
    println("Correct!")
    score++
} else {
    println("Sorry! The correct answer is $correctAnswer")
}
```

After iterating on the array, we can display the final score to the user by referring to the `score` variable. We can
also give a feedback on the user based on how they did on the questions.

```kotlin
println("Your final score is: $score out of ${questions.size}")
val feedback = when (score) {
    in 0..5 -> "Better luck next time!"
    in 6..9 -> "Not bad!"
    else -> "Awesome! You got a perfect score!"
}
println(feedback)
println("Thank you for playing!")
```

### Final Output

```kotlin
fun main() {
    // initialize array of questions
    val questions = arrayOf(
        questionCreator("What is Roger Federer's home country?", "France", "Switzerland", "Germany", "b"),
        questionCreator("Who has won the most women's Wimbledon singles titles?", "Martina Navratilova", "Serena Williams", "Chris Evert", "a"),
        questionCreator("How many French Open singles titles has Rafael Nadal won", "15", "14", "13", "c"),
        questionCreator("Who was the youngest and the first unseeded player to win the men's singles at Wimbledon?", "Michael Chang", "Boris Becker", "Jim Courier", "b"),
        questionCreator("Novak Djokovic turned professional in which year?", "2002", "2003", "2004", "b"),
        questionCreator("How many Grand Slams has Venus Williams won?", "7", "8", "9", "a"),
        questionCreator("Who won the women's French Open in 2019?", "Ashleigh Barty", "Serena Williams", "Naomi Osaka", "a"),
        questionCreator("Who ended a 77-year wait for a British men's Wimbledon champion in 2013?", "Cam Norrie", "Dan Evans", "Andy Murray", "c"),
        questionCreator("What is Rafael Nadal's hometown in Spain?", "Mallorca", "Barcelona", "Pamplona", "a"),
        questionCreator("Which player competed in the men’s singles finals match at the US Open 8 years consecutively?", "Boris Becker", "Ivan Lendl", "Bjorn Borg", "b"),
    )

    // user score
    var score = 0

    // loop over each question and ask the user
    questions.forEach { item ->
        // separate the values in item
        val question = item["question"]!!
        val a = item["a"]!!
        val b = item["b"]!!
        val c = item["c"]!!
        val correctAnswer = item["correct"]!!

        // prompt user with question
        val userAnswer = askQuestion(question, a, b, c)

        // check if user's answer is correct
        if (userAnswer == correctAnswer) {
            println("Correct!")
            score++
        } else {
            println("Sorry! The correct answer is $correctAnswer")
        }
        println() // add extra space
    }
    println("Your final score is: $score out of ${questions.size}")
    val feedback = when (score) {
        in 0..5 -> "Better luck next time!"
        in 6..9 -> "Not bad!"
        else -> "Awesome! You got a perfect score!"
    }
    println(feedback)
    println("Thank you for playing!")
}

fun askQuestion(question: String, a: String, b: String, c: String): String {
    // display the question
    println(question)
    println("(a) - $a")
    println("(b) - $b")
    println("(c) - $c")

    // get user answer
    val answer = readln().lowercase()
    return when {
        // recurse if invalid answer
        answer.isBlank() || answer.isEmpty() -> {
            println("Please enter an answer")
            askQuestion(question, a, b, c)
        }
        answer !in "abc" -> {
            println("Please select between a, b, or c")
            askQuestion(question, a, b, c)
        }

        // return if valid answer
        else -> {
            println("Your answer is: $answer")
            return answer.trim()
        }
    }
}

// function to create a question in the form of a map
fun questionCreator(
    question: String,
    a: String,
    b: String,
    c: String,
    correctAnswer: String
): Map<String, String> =
    mapOf(
        "question" to question,
        "a" to a,
        "b" to b,
        "c" to c,
        "correct" to correctAnswer
    )
```
