# Problem 2

---

Write a function that accepts a list as a parameter.

**Conditions:**
- The list contains string values of the colors of the rainbow. (roygbiv).

- Some of the colors can have duplicates in the list.

- Count the occurrence of each color, and return a list of mapped values.

- If the value of the string, is not in the colors of the rainbow, it will be counted under "Others"

The case of the string does not matter, meaning it should be able to process both uppercase, lowercase, and mix case. Ex:

```kotlin
// Sample Input:
listOf("red", "blue", "blue", "pink")

// Expected Output: 
// { red = 1, blue = 2, others = 1}
```

## Solution

---

We will define the function `countColors` that will accept a list of colors `List<String>` and it will return a
`Map<String, Int>` with `String` as its key and `Int` as its value.

```kotlin
fun countColors(colors: List<String>): Map<String, Int>
```

Moving on to the function body, the first thing that we'll do is to convert every string inside the `colors` parameter
into lowercase. This will allow us to be more consistent when it comes to checking the string values.

```kotlin
// lowercase the list first
val lowercased = colors.map { it.lowercase() }
```

Then, we will make a list of all the colors of the rainbow. That way, once we're iterating on the list parameter, we will
have a list to refer to if the color from the parameter is indeed part of the rainbow.

```kotlin
// reference list for colors of the rainbow
val rainbow = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")
```

After that, we will create a mutable map that will keep track of the number of colors that will appear in the list parameter.
Instead of typing each color one-by-one, we can simply refer to the previous list of colors of the rainbow and create a new map
using the list method `associateWith`.

This method will create an immutable map where the keys will be each item on the list.
As for the values, `associateWith` accepts a lambda function where the item of the list is the parameter (referred to by the `it`
keyword). For this function, we will ignore the `it` keyword and simply return 0 as the value.

You can learn more about `associateWith` by clicking **[here](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/associate-with.html)**

After using `associateWith`, we will also add another entry to the map that will keep count of all the colors that are **not**
part of the rainbow list. The key will be "other" and its value is also initialized as 0. We can add this entry to the map by simply
using the `+` operator.

Then, we will convert the map to a mutable one since the map that `associateWith` returns is immutable. This way, we can
modify the map while we are iterating over the lowercased colors list.

```kotlin
// create a map from rainbow list where the keys are the colors and
// the values are 0, then turn it to a mutable map
val countMap = (rainbow.associateWith { 0 } + mapOf("other" to 0)).toMutableMap()
```

We can now move on to the iteration. We will do this by using the `forEach` method. Remember that the list
we're going to be iterating over will be the `lowercased` one, not the `colors` parameter.

```kotlin
// iterate over lowercased list
lowercased.forEach 
```

For the lambda function, we can refer to each color of the list as simply `color`. Then, we will check if `color` exists
in the `rainbow` list. If it does, update the count of that color with + 1. We also need to assert that it is non-null
since the return type when accessing an entry of a map is always nullable.

```kotlin
 // check if color is in rainbow
if (color in rainbow)
    countMap[color] = countMap[color]!! + 1 // assert that value is not null
```

Else, if `color` does not exist in `rainbow`, then we add count to the "other" entry by accessing it's value and adding 1

```kotlin
else
    countMap["other"] = countMap["other"]!! + 1
```

After the iteration, we can filter the resulting map to exclude entries with 0 value using `filter`.

```kotlin
countMap.filter { it.value != 0 }
```

### Final Output

```kotlin
fun countColors(colors: List<String>): Map<String, Int> {
    // lowercase the list first
    val lowercased = colors.map { it.lowercase() }

    // reference list for colors of the rainbow
    val rainbow = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")

    // create a map from rainbow list where the keys are the colors and
    // the values are 0, then turn it to a mutable map
    val countMap = (rainbow.associateWith { 0 } + mapOf("other" to 0)).toMutableMap()

    // iterate over lowercased list
    lowercased.forEach { color ->
        // check if color is in rainbow
        if (color in rainbow)
            countMap[color] = countMap[color]!! + 1 // assert that value is not null
        else
            countMap["other"] = countMap["other"]!! + 1
    }
    return countMap.filter { it.value != 0 }
}
```