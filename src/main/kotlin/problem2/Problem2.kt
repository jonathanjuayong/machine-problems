package problem2

// Write a function that accepts a list as a parameter.
// Conditions:
//      - The list contains string values of the colors of the rainbow. (roygbiv).
//      - Some of the colors can have duplicates in the list.
//      - Count the occurrence of each color, and return a list of mapped values.
//      - If the value of the string, is not in the colors of the rainbow,
//        it will be counted under "Others"
//      - The case of the string does not matter,
//        meaning it should be able to process both uppercase, lowercase, and mix case.
//
//      Ex:
//      Sample Input: listOf("red", "blue", "blue", "pink")
//      Expected Output: { red = 1, blue = 2, others = 1}

fun main() {
    val colors = listOf(
        "Yellow", "Orange", "Orange", "Orange", "Yellow", "Red",
        "Red", "Red", "Yellow", "Violet", "Violet",
        "Violet", "Violet", "Indigo", "Indigo", "Red", "Red",
        "Indigo", "Red", "Indigo", "Brown", "pink",
        "Purple", "This", "Is", "Not", "a", "color"
    )
    println(countColors(colors))
}

fun countColors(colors: List<String>): Map<String, Int> {
    // lowercase the list first
    val lowercased = colors.map { it.lowercase() }

    // reference list for colors of the rainbow
    val rainbow = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")

    // create a map from rainbow list where the keys are the colors and
    // the values are 0, then turn it to a mutable map
    val countMap = (rainbow.associateWith { 0 } + mapOf("other" to 0)).toMutableMap()

    // iterate over lowercased list
    lowercased.forEach { color ->
        // check if color is in rainbow
        if (color in rainbow)
            countMap[color] = countMap[color]!! + 1 // assert that value is not null
        else
            countMap["other"] = countMap["other"]!! + 1
    }
    return countMap.filter { it.value != 0 }
}