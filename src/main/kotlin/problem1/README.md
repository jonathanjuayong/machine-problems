# Problem 1

---

Write a function that accepts a string as a parameter.

**Conditions:**

- It can only accept up to 15 characters.
- If the number of characters is even, return the string in reverse.
- If the number of characters is odd, return the string in alphabetical order.

## Solution

---

The first thing that we need to do here is to define the types of the function. We'll call it `processString`.
According to the problem, `processString` must accept a `String` type as a parameter. We can assume that it the return type must also be `String`

```kotlin
fun proccessString(string: String): String
```

After defining the type of the function, we can move on to the body.
Since the main thing that we're going to check in this function is the length of the `string`, we can declare a `val`
that contains the length of the `String` parameter. We do this by accessing the `length` property of the `String`

```kotlin
val length = string.length
```

Now that we have the `length`, we can do multiple checks on it by using the `when` keyword

```kotlin
return when {
        length >= 15 -> string // if more than 15 characters, simply return the string unchanged
        length % 2 == 0 -> string.reversed() // reverse if length is even
        else -> string.toList().sorted().joinToString("") // catchall condition
    }
```

First, we check if the `length` is less than or equal to 15. If it is, then we simply leave the string unchanged.

Then, if the length is even, we will reverse the string.

Finally, we can use the catch-all `else` condition to check if the string is odd. If it is, then we will rearrange the whole string
in alphabetical order.

### Final Output

```kotlin
fun processString(string: String): String {
    val length = string.length
    return when {
        length >= 15 -> string // if more than 15 characters, simply return the string unchanged
        length % 2 == 0 -> string.reversed() // reverse if length is even
        else -> string.toList().sorted().joinToString("") // catchall condition
    }
}
```

## Alternative Solution

---

We can also define a function `processStringOrNull` that will return a `null` if the string is over 15 characters.
We can do this by simply calling the previous function `processString` inside the new function and then comparing it to the string parameter.

Remember that the behavior of the original function is that it will return the string unchanged if the length is greater
than 15 characters. In that case, we can check if the returned string of `processString` is equal to the string parameter.
We return `null` if it's the same. Else, we return the result of calling `processString`

```kotlin
fun processStringOrNull(string: String): String? {
    val processedString = processString(string)

    // if string is unchanged, that means it's more than 15 chars
    return if (string == processedString)
        null
    else
        processedString
}
```