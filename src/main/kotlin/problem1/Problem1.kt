package problem1

// Write a function that accepts a string as a parameter.
// Conditions:
//
//    - It can only accept up to 15 characters.
//    - If the number of characters is even, return the string in reverse.
//    - If the number of characters is odd, return the string in alphabetical order.

fun main() {
    val test1 = "jonathan"
    val test2 = "suzette"
    val test3 = "jonathanrjuayong"
    val test4 = "jonathan123$#48"

    println(processString(test1)) // should be reversed
    println(processString(test2)) // should be alphabetized
    println(processString(test3)) // should be unchanged
    println(processStringOrNull(test3)) // should be null
    println(processString(test4)) // should be sorted
}

fun processString(string: String): String {
    val length = string.length
    return when {
        length >= 15 -> string // if more than 15 characters, simply return the string unchanged
        length % 2 == 0 -> string.reversed() // reverse if length is even
        else -> string.toList().sorted().joinToString("") // catchall condition
    }
}

fun processStringOrNull(string: String): String? {
    val processedString = processString(string)

    // if string is unchanged, that means it's more than 15 chars
    return if (string == processedString)
        null
    else
        processedString
}
